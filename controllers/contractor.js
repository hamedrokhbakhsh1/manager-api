const dotenv =require('dotenv').config();
const db = require('../public/db');
const sql = require('mssql');
const config = require('../public/dbConfig');
const jwt = require('jsonwebtoken');


sql.close();
exports.login =  (req, res, next) => {

    const username = req.body.username ;
    const password = req.body.password ;

    query = `select SysUser.Name, Ctrntor.ID from afw_SystemUsers SysUser left join krf_Contractors Ctrntor on Ctrntor.SystemUser = SysUser.ID where UserName = N'${username}' and LoginPassword ='${password}'`



    sql.connect(config.config).then(() => {
        return sql.query(query)
    }).then(result => {
        const send = [{
            token: '' ,
            Name: ''
        }];
        send[0].token = jwt.sign({userID: result.recordsets[0][0].ID } , process.env["PRIVATE_KEY"]);
        send[0].Name = result.recordsets[0][0].Name;
        res.status(200).json({
            status: 1 ,
            errorMessage: null ,
            data: send
        })
    }).catch(err => {
        res.status(201).json({
            status: 0 ,
            errorMessage: err ,
            data: 'not match'
        })
    })


    sql.on('error', err => {
        res.status(400).json({
            status: 0 ,
            errorMessage: err ,
            data: 'internal  connection'
        })
    });




}



exports.firstPage = (req, res, next) => {
    const userId = req.userData.userID ;
    const date = {
        from: req.body.from,
        until: req.body.until ,
    }
    const query = `select SUM(OneSessionAmount) as total, COUNT(SubServiceType) as num , SubServiceType from krf_RegistrationContractorPerformedServicesView where Contractor = '${userId}' AND  CAST(RegistrationDate as date) >='${date.from}' AND  CAST(RegistrationDate as date) <= '${date.until}' group by SubServiceType 
`



    let data = {
        RegisterNum: 0 ,
        RegisterTotalCost: 0 ,
        EducationNum: 0 ,
        EducationTotalCost: 0 ,
        SubServiceNum: 0 ,
        SubServiceTotalCost: 0 ,
    }

    sql.close();
    sql.connect(config.config).then(() => {
        return sql.query(query)
    }).then(result => {

        result.recordsets[0].forEach(element => {
            if (element.SubServiceType === 'Education'){
                data.RegisterNum  = element.num ;
                data.RegisterTotalCost = element.total ;
            }
            if (element.SubServiceType === 'Registration'){
                data.RegisterNum += element.num ;
                data.RegisterTotalCost += element.total ;
            }
            if (element.SubServiceType === 'SubService'){
                data.SubServiceNum = element.num ;
                data.SubServiceTotalCost = element.total ;
            }
        })

            res.status(200).json({
                status: 1,
                errorMessage: null,
                data: data
            })




    }).catch(err => {
        res.status(404).json({
            status: 0 ,
            errorMessage: err ,
            data: 'internal  connection'
        })
    })


    sql.on('error', err => {
        res.status(400).json({
            status: 0 ,
            errorMessage: err ,
            data: 'internal  connection'
        })
    });




}



exports.register  = (req, res, next) => {


    const userId = req.userData.userID ;
    const date = {
        from: req.body.from,
        until: req.body.until ,
    }

    const query = `select Service_Text, MemberFullName, OneSessionAmount
        from krf_RegistrationContractorPerformedServicesView  
        where ( SubServiceType = 'Registration' OR SubServiceType = 'Education' ) AND Contractor = '${userId}' AND  
        CAST(RegistrationDate as date) >='${date.from}' AND 
        CAST(RegistrationDate as date) <= '${date.until}'`;


    sql.close();
    sql.connect(config.config).then(() => {
        return sql.query(query)
    }).then(result => {
        res.status(200).json({
            status: 1 ,
            errorMessage: null ,
            data: result.recordsets[0]
        })
    }).catch(err => {
        res.status(201).json({
            status: 0 ,
            errorMessage: err ,
            data: 'internal  connection'
        })
    })


    sql.on('error', err => {
        res.status(400).json({
            status: 0 ,
            errorMessage: err ,
            data: 'internal  connection'
        })
    });

}





exports.single  = (req, res, next) => {
    const userId = req.userData.userID ;
    const date = {
        from: req.body.from,
        until: req.body.until ,
    }

    const query = `select Service_Text, MemberFullName, OneSessionAmount
        from krf_RegistrationContractorPerformedServicesView  
        where SubServiceType = 'SubService' AND Contractor = '${userId}' AND  
        CAST(RegistrationDate as date) >='${date.from}' AND 
        CAST(RegistrationDate as date) <= '${date.until}' 

`;  sql.close();
    sql.connect(config.config).then(() => {
        return sql.query(query)
    }).then(result => {
        res.status(200).json({
            status: 1 ,
            errorMessage: null ,
            data: result.recordsets[0]
        })
        sql.close();
    }).catch(err => {
        res.status(404).json({
            status: 0 ,
            errorMessage: err ,
            data: 'internal  connection'
        })
    })


    sql.on('error', err => {
        res.status(400).json({
            status: 0 ,
            errorMessage: err ,
            data: 'internal  connection'
        })
    });

}
