const express = require('express');
const router = express.Router();
const contractor = require('../controllers/contractor');
const checkAuth = require('../middleware/check-auth')


router.post('/login', contractor.login);



router.post('/first-page', checkAuth ,contractor.firstPage)



router.post('/register', checkAuth , contractor.register)


router.post('/single', checkAuth , contractor.single)



module.exports = router;
